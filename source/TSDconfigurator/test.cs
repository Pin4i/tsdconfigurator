﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TSDconfigurator
{
    public partial class test : Form
    {
        int i = 0;
        bool Restart = false;
        public test(string text, int time, bool restart)
        {
            InitializeComponent();
            i = time;
            Restart = restart;
            label1.Text = text;
            timer1.Interval = 1000;
            timer1.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (i <= 0)
            {
                if (Restart)
                {
                    new Reboot().ResetPocketPC();
                }
                else
                {
                    this.Close();
                }
            }
            else
            {
                i -= 1;
                label_i.Text = i.ToString(); ;
            }
        }
    }
}