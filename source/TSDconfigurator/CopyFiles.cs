﻿using System;

using System.Collections.Generic;
using System.Text;
using System.IO;

namespace TSDconfigurator
{
    public static class CopyFiles
    {
        public static void CopyToNand(string APP_PATH)
        {
            string appPath = APP_PATH.Substring(0, (APP_PATH.LastIndexOf('\\')));

            if (Directory.Exists("\\Nand\\DSICAPP"))
            {
                Directory.Delete("\\Nand\\DSICAPP", true);
            }

            if (Directory.Exists("\\Nand\\TSDconfigurator"))
            {
                Directory.Delete("\\Nand\\TSDconfigurator", true);
            }
            


            DeepCopy(new DirectoryInfo(appPath), new DirectoryInfo("\\Nand"));
        }

        public static void DeepCopy(DirectoryInfo source, DirectoryInfo target)
        {
            // Recursively call the DeepCopy Method for each Directory
            foreach (DirectoryInfo dir in source.GetDirectories())
            {
                DeepCopy(dir, target.CreateSubdirectory(dir.Name));
            }

            // Go ahead and copy each file in "source" to the "target" directory
            foreach (FileInfo file in source.GetFiles())
            {
                file.CopyTo(Path.Combine(target.FullName, file.Name));
            }
        }  
    }
}
