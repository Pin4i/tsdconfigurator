﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TSDconfigurator
{
    public partial class Form1 : Form
    {
        private System.Collections.ArrayList Items = new System.Collections.ArrayList();
        public static string APP_PATH = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
        private string CONF_ALL_DEVIVES = APP_PATH + @"\Configs\devices.txt";

        public Form1() // НЕ ЗАБУДЬ ПОМЕНЯТЬ SSID !!!
        {
            // НЕ ЗАБУДЬ ПОМЕНЯТЬ SSID !!!
            InitializeComponent();
            EnableWiFi.Start();
            GetItems();
        }

        private void ExeQuerrySilence()
        {
            ExeQuerry.Start();
        }

        private void GetItems()
        {
            if(System.IO.File.Exists(CONF_ALL_DEVIVES))
            {    
                using (System.IO.StreamReader reader = new System.IO.StreamReader(CONF_ALL_DEVIVES))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                     string[] name_and_path = line.Split('|');
                     Items.Add(new item(name_and_path[0], name_and_path[1]));
                    }
                    BindDataGridToArrayList();
                }
            }
            else
            {
                if(System.IO.Directory.Exists(APP_PATH+@"\Configs"))
                {
                    CreateConfigFile();
                }
                else
                {
                    System.IO.Directory.CreateDirectory(APP_PATH + @"\Configs");
                    CreateConfigFile();
                }
                MessageBox.Show(@"See: AppFolder\Configs\devices.txt!");
            }              
        }

        private void CreateConfigFile()
        {
            System.IO.File.Create(CONF_ALL_DEVIVES).Close();
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(CONF_ALL_DEVIVES))
            {
                sw.Write("EXAMPLE: TextForDisplay|192.168.0.100      PS. SYMBOL | MUST HAVE FOR WORKING SOFTWARE!");
            }
        }

        private void BindDataGridToArrayList()
        {
            dataGrid1.DataSource = Items;

            DataGridTableStyle ts = new DataGridTableStyle();
            ts.MappingName = "ArrayList";

            DataGridTextBoxColumn cs1 = new DataGridTextBoxColumn();
            cs1.Width = 106;
            cs1.MappingName = "Name";
            cs1.HeaderText = "TSD Name";
            ts.GridColumnStyles.Add(cs1);

            DataGridTextBoxColumn cs2 = new DataGridTextBoxColumn();
            cs2.Width = 106;
            cs2.MappingName = "Path";
            cs2.HeaderText = "IP";
            ts.GridColumnStyles.Add(cs2);            

            dataGrid1.TableStyles.Clear();
            dataGrid1.TableStyles.Add(ts);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Items.Add(new item("AA-909", "Camera #3"));
            CurrencyManager cm = (CurrencyManager)this.dataGrid1.BindingContext[Items];

            if (cm != null)
                cm.Refresh();
        }

        public struct item
        {
            private string Name_; // name
            private string Path_; // path

            public item(string Name, string Path)
            {
                Name_ = Name;
                Path_ = Path;
            }
            public string Name
            {
                get { return Name_; }
                set { Name_ = value; }
            }
            public string Path
            {
                get { return Path_; }
                set { Path_ = value; }
            }            
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //System.IO.File.Create(APP_PATH + @"\Configs\BackLight.txt").Close();

//            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(APP_PATH + @"\Configs\BackLight.txt"))
//            {
//                sw.Write(
//"[HKEY_CURRENT_USER\\Software\\ClientTermWMS]" +"\n"
//+ "\"Server\"=\"192.168.0.117\"" + "\n"
//+ "\"Passw\"=\"6736\"" + "\n"
//+ "TimeoutRead\"=\"3000" + "\n"
//+ "TimeoutConnect\"=\"3000" + "\n"
//+ "Port\"=\"20" + "\n"
//+ "Save4\"=hex:FC,7E,36,3A,BF,AF,76,67,7E,AF,A6,C9,F3,80,0E,CB,FC,58,1B,11,5B,81,D4,\\" + "\n"
//+ "  54,F8,D4,7A,5B,4A,9D,71,BA,AC,52,CC,CA,ED,15,8A,1C,AB,FF,75,3D,00,32,27,79,\\" + "\n"
//+ "  3D,C1,78,90,26,56,75,62,67,2D,0A,5A,36,13,7A,C3,D8,B2,C0,EF,78,0B,59,00,CD,\\" + "\n"
//+ "  53,10,DD,6C,62,19,3D,F6,A1,ED,70,E4,03,4E,4A,17,E3,FE,80,24,B8,E4,AF,39,F8,\\" + "\n"
//+ "  05,7F,69,79,E5,F4,96,61,FE,7E,E0,39,8A,3E,D8,10,53,48,5A,F1,8F,37,53,2E,88,\\" + "\n"
//+"  03,5F,C2,F5,C4");
//            }
            Application.Exit();
        }

        private void CreateErrorsFile(string exeption)
        {
            if(!System.IO.File.Exists(APP_PATH+@"\Errors.txt"))
            {
                System.IO.File.Create(APP_PATH+@"\Errors.txt").Close();
            }
            
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(APP_PATH + @"\Errors.txt"))
            {
                sw.Write(DateTime.Now + ": " + exeption);
            }
        }

        private void dataGrid1_Click(object sender, EventArgs e)
        {
            try
            {
                label_tsd.Text = "Подтвердите выбор: " + dataGrid1[dataGrid1.CurrentCell.RowNumber, dataGrid1.CurrentCell.ColumnNumber].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Something wrong. See: appFolder\Errors.txt");
                CreateErrorsFile(ex.ToString());
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ExeQuerrySilence();

            if (cb.Checked)
            {
                if ((APP_PATH.IndexOf("Nand")) >= 0)
                {
                    MessageBox.Show("Программа находится в Nand!\nДля копирования файлов\nпрограмма должна быть на флешке");
                }
                else
                {
                    CopyFiles.CopyToNand(APP_PATH);
                }                
            }
            Reboot reboot = new Reboot();

            string clientName = dataGrid1[dataGrid1.CurrentCell.RowNumber, dataGrid1.CurrentCell.ColumnNumber].ToString();
            string clientIp = dataGrid1[dataGrid1.CurrentCell.RowNumber, dataGrid1.CurrentCell.ColumnNumber + 1].ToString();;


            BlackL.start();
            wms.start();
            tcpip.start(clientIp);
            Ssid.Start(clientName);

            test tform = new test("Устройство перезагружается...", 3 , true);
            tform.Show();   
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //if ((APP_PATH.IndexOf("Nand")) >= 0)
            //{
            //    cb.Checked = false;
            //}
            //else
            //{
            //    cb.Checked = true;
            //}
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            
        }
    }
}