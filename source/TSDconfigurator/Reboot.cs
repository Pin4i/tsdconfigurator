﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace TSDconfigurator
{
    public class Reboot
    {
        public const uint FILE_DEVICE_HAL = 0x00000101;
        public const uint METHOD_BUFFERED = 0;
        public const uint FILE_ANY_ACCESS = 0;

        public uint CTL_CODE(uint DeviceType, uint Function, uint Method, uint Access)
        {
            return ((DeviceType << 16) | (Access << 14) | (Function << 2) | Method);
        }

        [DllImport("Coredll.dll")]
        public extern static uint KernelIoControl
        (
            uint dwIoControlCode,
            IntPtr lpInBuf,
            uint nInBufSize,
            IntPtr lpOutBuf,
            uint nOutBufSize,
            ref uint lpBytesReturned
        );

        public uint ResetPocketPC()
        {
            uint bytesReturned = 0;
            uint IOCTL_HAL_REBOOT = CTL_CODE(FILE_DEVICE_HAL, 15,
              METHOD_BUFFERED, FILE_ANY_ACCESS);
            return KernelIoControl(IOCTL_HAL_REBOOT, IntPtr.Zero, 0,
              IntPtr.Zero, 0, ref bytesReturned);
        }
    }
}
