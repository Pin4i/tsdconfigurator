﻿using System;

using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace TSDconfigurator
{
    public static class ExeQuerry
    {
        public static void Start()
        {
            RegistryKey key = null;
        
            if ((Registry.LocalMachine.OpenSubKey("Security\\Policies\\Policies") != null))
            {
                key = Registry.LocalMachine.OpenSubKey("Security\\Policies\\Policies", true);

                key.SetValue("0000101a", 1, RegistryValueKind.DWord);
            }
            else
            {
                key = Registry.LocalMachine.CreateSubKey("Security\\Policies\\Policies");

                key.SetValue("0000101a", 1, RegistryValueKind.DWord);
            }
        }
    }         
}
