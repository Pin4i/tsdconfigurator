﻿using System;

using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace TSDconfigurator
{
    public static class tcpip
    {
        public static void start(string ip_address)
        {
            RegistryKey key = null;

            if ((Registry.LocalMachine.OpenSubKey("Comm\\SDCSD40N1\\Parms\\Tcpip") != null))
            {
                key = Registry.LocalMachine.OpenSubKey("Comm\\SDCSD40N1\\Parms\\Tcpip", true);

                key.SetValue("WINS", new string[]{""}, RegistryValueKind.MultiString);
                key.SetValue("DNS", new string[]{"192.168.109.3"}, RegistryValueKind.MultiString);
                key.SetValue("DefaultGateway", new string[]{"192.168.0.58"}, RegistryValueKind.MultiString);
                key.SetValue("Subnetmask", new string[]{"255.255.248.0"}, RegistryValueKind.MultiString);
                key.SetValue("IpAddress", new string[]{ip_address}, RegistryValueKind.MultiString);

                key.SetValue("EnableDHCP", 0, RegistryValueKind.DWord);
                //key.SetValue("LeaseObtainedHigh", 30271595, RegistryValueKind.DWord);
                //key.SetValue("LeaseObtainedLow", 247242880, RegistryValueKind.DWord);
                //key.SetValue("AutoInterval", 300, RegistryValueKind.DWord);

                //key.SetValue("AutoMask", new string[]{"255.255.0.0"}, RegistryValueKind.MultiString);
                //key.SetValue("AutoSubnet", new string[]{"169.254.0.0"}, RegistryValueKind.MultiString);

                //key.SetValue("AutoSeed", 0, RegistryValueKind.DWord);

                //byte[] b = { 0x07, 0x01, 0x03, 0x06, 0x0F, 0x2C, 0x2E, 0x2F };
                //key.SetValue("PrevReqOptions", b, RegistryValueKind.Binary);

                //key.SetValue("T2", 3150, RegistryValueKind.DWord);
                //key.SetValue("T1", 1800, RegistryValueKind.DWord);
                //key.SetValue("Lease", 3600, RegistryValueKind.DWord);

                //key.SetValue("DhcpDNS", new string[]{"192.168.43.1"}, RegistryValueKind.MultiString);
                //key.SetValue("DhcpServer", new string[]{"192.168.43.1"}, RegistryValueKind.MultiString);
                //key.SetValue("DhcpSubnetMask", new string[]{"255.255.255.0"}, RegistryValueKind.MultiString);
                //key.SetValue("DhcpIPAddress", new string[]{"192.168.43.1"}, RegistryValueKind.MultiString);

                key.SetValue("AutoCfg", 1, RegistryValueKind.DWord);
                key.SetValue("TcpMaxConnectResponseRetransmissions", 3, RegistryValueKind.DWord);
                key.SetValue("TcpInitialRTT", 1000, RegistryValueKind.DWord);
                key.SetValue("DisconnectDampInterval", 0, RegistryValueKind.DWord);
                key.SetValue("DhcpRetryDialogue", 60000, RegistryValueKind.DWord);
                key.SetValue("DhcpMaxRetry", 60000, RegistryValueKind.String);
                key.SetValue("DhcpInitDelayInterval", 1000, RegistryValueKind.String);
                key.SetValue("ConnectDampingInterval", 1, RegistryValueKind.String);
            }

            else
            {
                key = Registry.LocalMachine.CreateSubKey("Comm\\SDCSD40N1\\Parms\\Tcpip");

                key.SetValue("WINS", new string[] { "" }, RegistryValueKind.MultiString);
                key.SetValue("DNS", new string[] { "192.168.109.3" }, RegistryValueKind.MultiString);
                key.SetValue("DefaultGateway", new string[] { "192.168.0.58" }, RegistryValueKind.MultiString);
                key.SetValue("Subnetmask", new string[] { "255.255.248.0" }, RegistryValueKind.MultiString);
                key.SetValue("IpAddress", new string[] { ip_address }, RegistryValueKind.MultiString);

                key.SetValue("EnableDHCP", 0, RegistryValueKind.DWord);
                //key.SetValue("LeaseObtainedHigh", 30271595, RegistryValueKind.DWord);
                //key.SetValue("LeaseObtainedLow", 247242880, RegistryValueKind.DWord);
                //key.SetValue("AutoInterval", 300, RegistryValueKind.DWord);

                //key.SetValue("AutoMask", new string[]{"255.255.0.0"}, RegistryValueKind.MultiString);
                //key.SetValue("AutoSubnet", new string[]{"169.254.0.0"}, RegistryValueKind.MultiString);

                //key.SetValue("AutoSeed", 0, RegistryValueKind.DWord);

                //byte[] b = { 0x07, 0x01, 0x03, 0x06, 0x0F, 0x2C, 0x2E, 0x2F };
                //key.SetValue("PrevReqOptions", b, RegistryValueKind.Binary);

                //key.SetValue("T2", 3150, RegistryValueKind.DWord);
                //key.SetValue("T1", 1800, RegistryValueKind.DWord);
                //key.SetValue("Lease", 3600, RegistryValueKind.DWord);

                //key.SetValue("DhcpDNS", new string[]{"192.168.43.1"}, RegistryValueKind.MultiString);
                //key.SetValue("DhcpServer", new string[]{"192.168.43.1"}, RegistryValueKind.MultiString);
                //key.SetValue("DhcpSubnetMask", new string[]{"255.255.255.0"}, RegistryValueKind.MultiString);
                //key.SetValue("DhcpIPAddress", new string[]{"192.168.43.1"}, RegistryValueKind.MultiString);

                key.SetValue("AutoCfg", 1, RegistryValueKind.DWord);
                key.SetValue("TcpMaxConnectResponseRetransmissions", 3, RegistryValueKind.DWord);
                key.SetValue("TcpInitialRTT", 1000, RegistryValueKind.DWord);
                key.SetValue("DisconnectDampInterval", 0, RegistryValueKind.DWord);
                key.SetValue("DhcpRetryDialogue", 60000, RegistryValueKind.DWord);
                key.SetValue("DhcpMaxRetry", 60000, RegistryValueKind.String);
                key.SetValue("DhcpInitDelayInterval", 1000, RegistryValueKind.String);
                key.SetValue("ConnectDampingInterval", 1, RegistryValueKind.String);
            }

        }
    }
}
