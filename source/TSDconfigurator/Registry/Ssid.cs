﻿using System;

using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace TSDconfigurator
{
    public static class Ssid
    {
        public static void Start(string clientName)
        {
            if ((Registry.LocalMachine.OpenSubKey("Comm\\SDCCF10G1\\Parms\\Configs\\Config01") != null))
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey("Comm\\SDCCF10G1\\Parms\\Configs\\Config01", true);

                byte[] f4 = {0xFC,0x7E,0x36,0x3A,0xBF,0xAF,0x76,0x67,0x7E,0xAF,0xA6,0xC9,0xF3,0x80,0x0E,0xCB,0xFC,0x58,0x1B,0x11,0x5B,0x81,0xD4,0x54,0xF8,0xD4,0x7A,0x5B,0x4A,0x9D,0x71,0xBA,0xAC,0x52,0xCC,0xCA,0xED,0x15,0x8A,0x1C,0xAB,0xFF,0x75,0x3D,0x00,0x32,0x27,0x79,0x3D,0xC1,0x78,0x90,0x26,0x56,0x75,0x62,0x67,0x2D,0x0A,0x5A,0x36,0x13,0x7A,0xC3,0xD8,0xB2,0xC0,0xEF,0x78,0x0B,0x59,0x00,0xCD,0x53,0x10,0xDD,0x6C,0x62,0x19,0x3D,0xF6,0xA1,0xED,0x70,0xE4,0x03,0x4E,0x4A,0x17,0xE3,0xFE,0x80,0x24,0xB8,0xE4,0xAF,0x39,0xF8,0x05,0x7F,0x69,0x79,0xE5,0xF4,0x96,0x61,0xFE,0x7E,0xE0,0x39,0x8A,0x3E,0xD8,0x10,0x53,0x48,0x5A,0xF1,0x8F,0x37,0x53,0x2E,0x88,0x03,0x5F,0xC2,0xF5,0xC4 };
                key.SetValue("Save4", f4, RegistryValueKind.Binary);

                byte[] f3 = {0xAF,0x58,0x90,0xC6,0x63,0xA7,0xF9,0x06,0xCB,0x53,0x62,0xD7,0xDC,0x34,0xED,0xC1,0x70,0x1E,0x76,0x55,0x08,0x8C,0x57,0xE1,0x98,0x6D,0x8A,0xE5,0x45,0xEE,0xD0,0xA5,0xFD,0x84,0xA4,0x63,0x30,0x9B,0xE1,0x88,0x07,0x68,0x0D,0x96,0xE2,0x39,0xB2,0x64,0xF1,0xC6,0x21,0xCF,0xC8,0x94,0x92,0xA4,0x91,0x4D,0x1D,0x7A,0x54,0x88,0x1B,0x77,0xA6,0x09,0x4B,0x5D,0xF5,0xE9,0x4C,0x8E,0x82,0x7D,0x3F,0x74,0x40,0xCB,0xD0,0x0F,0xC2,0x5A,0x28,0xC0,0x28,0x29,0x04,0x57,0x00,0x44,0x24,0x98,0x8D,0x72,0x6C,0xFB,0x59,0x02,0xEA,0x74,0xCA,0x97,0x04,0x40,0x3F,0xAA,0xE0,0x41,0x57,0x1F,0x49,0xD3,0xC6,0x32,0x45,0x80,0x66,0x54,0x2F,0x21,0x10,0x10,0xD5,0xC7,0x08,0x33,0x82,0xE9};
                key.SetValue("Save3", f3, RegistryValueKind.Binary);

                byte[] f2 = {0x8E,0x87,0xEB,0x70,0xFB,0x9B,0x94,0x0C,0x8C,0xA9,0x55,0xF8,0x99,0x28,0x0F,0xBD,0x20,0x7C,0x1C,0xF7,0x35,0xDD,0x1C,0xE7,0xA9,0xC6,0x07,0xCA,0x80,0xF9,0x83,0x66,0x07,0xB1,0x11,0x5B,0xC6,0xA0,0x5B,0xF4,0xF3,0xCA,0xD5,0x60,0x2F,0x19,0x47,0xD5,0xD5,0x12,0x1D,0x43,0xF0,0x79,0x8D,0x42,0x8C,0xF1,0xC9,0x38,0x00,0x7E,0xFD,0x0C,0x9A,0xFE,0xFB,0xD6,0x93,0xF5,0x25,0xFA,0x35,0xD8,0x6B,0x8A,0xFA,0x05,0x64,0xBE,0xE7,0x61,0x52,0xB4,0x89,0xCA,0x45,0x88,0xEE,0xDE,0x05,0xFD,0xC8,0x5B,0xD3,0xF5,0xDD,0x18,0x3E,0xCE,0x93,0xEB,0x95,0xD9,0xFA,0xBD,0x1F,0x17,0xB9,0xE4,0x6A,0x09,0x6E,0x40,0x78,0x19,0x11,0x20,0x7D,0x13,0x58,0x75,0x80,0xBD,0xE0,0xD6,0xE3,0x65 };
                key.SetValue("Save2", f2, RegistryValueKind.Binary);

                byte[] f1 = {0xE4, 0xEF, 0xAE, 0x1B, 0xDF, 0x81, 0x56, 0x72, 0xCC, 0x75, 0xB5, 0x43, 0x42, 0x53, 0xE6, 0x91, 0x51, 0x52, 0xA8, 0x35, 0x31, 0x04, 0xFA, 0x3F, 0x9D, 0x3D, 0x4D, 0xDB, 0xAC, 0x44, 0x1E, 0xB4, 0x9F, 0x1D, 0x21, 0xDB, 0xB2, 0x2E, 0x2D, 0x68, 0x8D, 0x51, 0x92, 0x5D, 0x1F, 0x02, 0x1B, 0x5B, 0x55, 0x09, 0x84, 0xE0, 0x33, 0xEA, 0x9B, 0xA5, 0xE3, 0xEB, 0x46, 0x51, 0x72, 0x15, 0x19, 0x0F, 0x36, 0x4F, 0x4C, 0xC1, 0x5A, 0x17, 0xDF, 0x0C, 0xC5, 0x62, 0xBD, 0x3A, 0x49, 0xDF, 0x72, 0x21, 0x2A, 0x67, 0x2D, 0xF9, 0x8A, 0xA7, 0x03, 0xD5, 0x07, 0xC1, 0xE2, 0xAF, 0x1A, 0xE8, 0xC1, 0xDE, 0xC7, 0xEB, 0x18, 0x4F, 0xC1, 0x2C, 0x04, 0x96, 0xDF, 0xD1, 0xB7, 0x16, 0x69, 0x1A, 0x42, 0xDB, 0xDC, 0x74, 0xCA, 0x68, 0x14, 0xED, 0x7B, 0x14, 0xD6, 0xB9, 0xC8, 0x6C, 0xCC, 0xBD, 0xEF, 0x61 };
                key.SetValue("Save1", f1, RegistryValueKind.Binary);

                key.SetValue("RadioMode", 5, RegistryValueKind.DWord);
                key.SetValue("BitRate", 0, RegistryValueKind.DWord);
                key.SetValue("WEPType", 5, RegistryValueKind.DWord);
                key.SetValue("PowerSave", 2, RegistryValueKind.DWord);
                key.SetValue("EAPType", 0, RegistryValueKind.DWord);
                key.SetValue("AuthType", 0, RegistryValueKind.DWord);
                key.SetValue("TxPower", 0, RegistryValueKind.DWord);
                key.SetValue("AuthType", 0, RegistryValueKind.DWord);
                key.SetValue("ClientName", clientName, RegistryValueKind.String);
                key.SetValue("SSID", "   ***   ", RegistryValueKind.String);
                key.SetValue("ConfigName", "   ***   ", RegistryValueKind.String);
            }
            else
            {
                RegistryKey key = Registry.LocalMachine.CreateSubKey("Comm\\SDCCF10G1\\Parms\\Configs\\Config01");
                
                byte[] f4 = { 0xFC, 0x7E, 0x36, 0x3A, 0xBF, 0xAF, 0x76, 0x67, 0x7E, 0xAF, 0xA6, 0xC9, 0xF3, 0x80, 0x0E, 0xCB, 0xFC, 0x58, 0x1B, 0x11, 0x5B, 0x81, 0xD4, 0x54, 0xF8, 0xD4, 0x7A, 0x5B, 0x4A, 0x9D, 0x71, 0xBA, 0xAC, 0x52, 0xCC, 0xCA, 0xED, 0x15, 0x8A, 0x1C, 0xAB, 0xFF, 0x75, 0x3D, 0x00, 0x32, 0x27, 0x79, 0x3D, 0xC1, 0x78, 0x90, 0x26, 0x56, 0x75, 0x62, 0x67, 0x2D, 0x0A, 0x5A, 0x36, 0x13, 0x7A, 0xC3, 0xD8, 0xB2, 0xC0, 0xEF, 0x78, 0x0B, 0x59, 0x00, 0xCD, 0x53, 0x10, 0xDD, 0x6C, 0x62, 0x19, 0x3D, 0xF6, 0xA1, 0xED, 0x70, 0xE4, 0x03, 0x4E, 0x4A, 0x17, 0xE3, 0xFE, 0x80, 0x24, 0xB8, 0xE4, 0xAF, 0x39, 0xF8, 0x05, 0x7F, 0x69, 0x79, 0xE5, 0xF4, 0x96, 0x61, 0xFE, 0x7E, 0xE0, 0x39, 0x8A, 0x3E, 0xD8, 0x10, 0x53, 0x48, 0x5A, 0xF1, 0x8F, 0x37, 0x53, 0x2E, 0x88, 0x03, 0x5F, 0xC2, 0xF5, 0xC4 };
                key.SetValue("Save4", f4, RegistryValueKind.Binary);

                byte[] f3 = { 0xAF, 0x58, 0x90, 0xC6, 0x63, 0xA7, 0xF9, 0x06, 0xCB, 0x53, 0x62, 0xD7, 0xDC, 0x34, 0xED, 0xC1, 0x70, 0x1E, 0x76, 0x55, 0x08, 0x8C, 0x57, 0xE1, 0x98, 0x6D, 0x8A, 0xE5, 0x45, 0xEE, 0xD0, 0xA5, 0xFD, 0x84, 0xA4, 0x63, 0x30, 0x9B, 0xE1, 0x88, 0x07, 0x68, 0x0D, 0x96, 0xE2, 0x39, 0xB2, 0x64, 0xF1, 0xC6, 0x21, 0xCF, 0xC8, 0x94, 0x92, 0xA4, 0x91, 0x4D, 0x1D, 0x7A, 0x54, 0x88, 0x1B, 0x77, 0xA6, 0x09, 0x4B, 0x5D, 0xF5, 0xE9, 0x4C, 0x8E, 0x82, 0x7D, 0x3F, 0x74, 0x40, 0xCB, 0xD0, 0x0F, 0xC2, 0x5A, 0x28, 0xC0, 0x28, 0x29, 0x04, 0x57, 0x00, 0x44, 0x24, 0x98, 0x8D, 0x72, 0x6C, 0xFB, 0x59, 0x02, 0xEA, 0x74, 0xCA, 0x97, 0x04, 0x40, 0x3F, 0xAA, 0xE0, 0x41, 0x57, 0x1F, 0x49, 0xD3, 0xC6, 0x32, 0x45, 0x80, 0x66, 0x54, 0x2F, 0x21, 0x10, 0x10, 0xD5, 0xC7, 0x08, 0x33, 0x82, 0xE9 };
                key.SetValue("Save3", f3, RegistryValueKind.Binary);

                byte[] f2 = { 0x8E, 0x87, 0xEB, 0x70, 0xFB, 0x9B, 0x94, 0x0C, 0x8C, 0xA9, 0x55, 0xF8, 0x99, 0x28, 0x0F, 0xBD, 0x20, 0x7C, 0x1C, 0xF7, 0x35, 0xDD, 0x1C, 0xE7, 0xA9, 0xC6, 0x07, 0xCA, 0x80, 0xF9, 0x83, 0x66, 0x07, 0xB1, 0x11, 0x5B, 0xC6, 0xA0, 0x5B, 0xF4, 0xF3, 0xCA, 0xD5, 0x60, 0x2F, 0x19, 0x47, 0xD5, 0xD5, 0x12, 0x1D, 0x43, 0xF0, 0x79, 0x8D, 0x42, 0x8C, 0xF1, 0xC9, 0x38, 0x00, 0x7E, 0xFD, 0x0C, 0x9A, 0xFE, 0xFB, 0xD6, 0x93, 0xF5, 0x25, 0xFA, 0x35, 0xD8, 0x6B, 0x8A, 0xFA, 0x05, 0x64, 0xBE, 0xE7, 0x61, 0x52, 0xB4, 0x89, 0xCA, 0x45, 0x88, 0xEE, 0xDE, 0x05, 0xFD, 0xC8, 0x5B, 0xD3, 0xF5, 0xDD, 0x18, 0x3E, 0xCE, 0x93, 0xEB, 0x95, 0xD9, 0xFA, 0xBD, 0x1F, 0x17, 0xB9, 0xE4, 0x6A, 0x09, 0x6E, 0x40, 0x78, 0x19, 0x11, 0x20, 0x7D, 0x13, 0x58, 0x75, 0x80, 0xBD, 0xE0, 0xD6, 0xE3, 0x65 };
                key.SetValue("Save2", f2, RegistryValueKind.Binary);

                byte[] f1 = { 0xE4, 0xEF, 0xAE, 0x1B, 0xDF, 0x81, 0x56, 0x72, 0xCC, 0x75, 0xB5, 0x43, 0x42, 0x53, 0xE6, 0x91, 0x51, 0x52, 0xA8, 0x35, 0x31, 0x04, 0xFA, 0x3F, 0x9D, 0x3D, 0x4D, 0xDB, 0xAC, 0x44, 0x1E, 0xB4, 0x9F, 0x1D, 0x21, 0xDB, 0xB2, 0x2E, 0x2D, 0x68, 0x8D, 0x51, 0x92, 0x5D, 0x1F, 0x02, 0x1B, 0x5B, 0x55, 0x09, 0x84, 0xE0, 0x33, 0xEA, 0x9B, 0xA5, 0xE3, 0xEB, 0x46, 0x51, 0x72, 0x15, 0x19, 0x0F, 0x36, 0x4F, 0x4C, 0xC1, 0x5A, 0x17, 0xDF, 0x0C, 0xC5, 0x62, 0xBD, 0x3A, 0x49, 0xDF, 0x72, 0x21, 0x2A, 0x67, 0x2D, 0xF9, 0x8A, 0xA7, 0x03, 0xD5, 0x07, 0xC1, 0xE2, 0xAF, 0x1A, 0xE8, 0xC1, 0xDE, 0xC7, 0xEB, 0x18, 0x4F, 0xC1, 0x2C, 0x04, 0x96, 0xDF, 0xD1, 0xB7, 0x16, 0x69, 0x1A, 0x42, 0xDB, 0xDC, 0x74, 0xCA, 0x68, 0x14, 0xED, 0x7B, 0x14, 0xD6, 0xB9, 0xC8, 0x6C, 0xCC, 0xBD, 0xEF, 0x61 };
                key.SetValue("Save1", f1, RegistryValueKind.Binary);

                key.SetValue("RadioMode", 5, RegistryValueKind.DWord);
                key.SetValue("BitRate", 0, RegistryValueKind.DWord);
                key.SetValue("WEPType", 0, RegistryValueKind.DWord);
                key.SetValue("PowerSave", 2, RegistryValueKind.DWord);
                key.SetValue("EAPType", 0, RegistryValueKind.DWord);
                key.SetValue("AuthType", 0, RegistryValueKind.DWord);
                key.SetValue("TxPower", 0, RegistryValueKind.DWord);
                key.SetValue("AuthType", 0, RegistryValueKind.DWord);
                key.SetValue("ClientName", clientName, RegistryValueKind.String);
                key.SetValue("SSID", "   ***   ", RegistryValueKind.String);
                key.SetValue("ConfigName", "   ***   ", RegistryValueKind.String);
            }
        }
    }
}
