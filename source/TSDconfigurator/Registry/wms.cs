﻿using System;

using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace TSDconfigurator
{
    public static class wms
    {
        public static void start()
        {
            RegistryKey key = null;

            if ((Registry.CurrentUser.OpenSubKey("Software\\ClientTermWMS") != null))
            {
                key = Registry.CurrentUser.OpenSubKey("Software\\ClientTermWMS", true);

                key.SetValue("Server", "192.168.0.117", RegistryValueKind.String);
                key.SetValue("Passw", "6736", RegistryValueKind.String);
                key.SetValue("TimeoutRead", "3000", RegistryValueKind.String);
                key.SetValue("TimeoutConnect", "3000", RegistryValueKind.String);
                key.SetValue("Port", "20", RegistryValueKind.String);
            }

            else
            {
                key = Registry.CurrentUser.CreateSubKey("Software\\ClientTermWMS");

                key.SetValue("Server", "192.168.0.117", RegistryValueKind.String);
                key.SetValue("Passw", "6736", RegistryValueKind.String);
                key.SetValue("TimeoutRead", "3000", RegistryValueKind.String);
                key.SetValue("TimeoutConnect", "3000", RegistryValueKind.String);
                key.SetValue("Port", "20", RegistryValueKind.String);
            }
        }
    }
}
