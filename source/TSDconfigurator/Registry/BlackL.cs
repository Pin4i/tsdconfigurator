﻿using System;

using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace TSDconfigurator
{
    public static class BlackL
    {
        
        public static void start()
        {
            RegistryKey key = null;


            ////////////////////////////////////////////////            
            if ((Registry.LocalMachine.OpenSubKey("System\\CurrentControlSet\\Control\\Power\\Timeouts") != null))
            {
                key = Registry.LocalMachine.OpenSubKey("System\\CurrentControlSet\\Control\\Power\\Timeouts", true);
                                
                key.SetValue("ACSuspendTimeout", 0, RegistryValueKind.DWord);         
                key.SetValue("BattSuspendTimeout", 0, RegistryValueKind.DWord);
            }
            else
            {
                key = Registry.LocalMachine.CreateSubKey("System\\CurrentControlSet\\Control\\Power\\Timeouts");

                key.SetValue("ACSuspendTimeout", 0, RegistryValueKind.DWord);
                key.SetValue("BattSuspendTimeout", 0, RegistryValueKind.DWord);
                

            }            
        }
    }
}
