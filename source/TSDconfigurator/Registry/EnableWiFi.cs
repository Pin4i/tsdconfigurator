﻿using System;

using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace TSDconfigurator
{
    public static class EnableWiFi
    {
        public static void Start()
        {
            if ((Registry.LocalMachine.OpenSubKey("DSIC\\WirelessPower") != null))
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey("DSIC\\WirelessPower", true);
                
                try
                {    
                    key.SetValue("WLANPower", 1, RegistryValueKind.DWord);
                }

                catch(Exception)
                {
                    key.SetValue("WLANPower", 0, RegistryValueKind.DWord);
                    key.SetValue("WLANPower", 1, RegistryValueKind.DWord);
                }                            
            }

            else
            {
                RegistryKey key = Registry.LocalMachine.CreateSubKey("DSIC\\WirelessPower");
                key.SetValue("WLANPower", 1, RegistryValueKind.DWord);
            }

            test testForm = new test("Идет настройка...", 5, false);
            testForm.Show();
        }
    }
}
