﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ChangeTime
{
    public partial class timeForm : Form
    {
        public timeForm(DateTime time)
        {
            InitializeComponent();
            label1.Text = time.ToString();
        }
    }
}